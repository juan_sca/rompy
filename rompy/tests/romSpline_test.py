import sys, numpy as np, matplotlib.pyplot as plt
sys.path.append('../../')
import romgw as rom

Msun = rom.Msuninsec
Mtot = 6.*Msun
q = 1.
Mc = rom.Mq_to_Mc(Mtot, q)
nu = rom.q_to_nu(q)

fmin = 40.
fmax = 1000. #rom.fgwisco(Mtot)
frate = 2**12
inner = rom.Integration([fmin, fmax], rate=frate, rule='trapezoidal')
fs = inner.nodes

template_phi = rom.pnspa.Phase()
def phi(f, Mc, nu):
	return template_phi([Mc, nu, f], '3.5')

template_amp = rom.pnspa.Amplitude()
amp_norm = inner.norm(template_amp([Mc,fs], '0'))
def amp(f, Mc):
  return template_amp([Mc, f], '0')/amp_norm

rs_phi = rom.romSpline.ReducedOrderSpline(fs, phi(fs, Mc, nu), verbose=True)
rs_amp = rom.romSpline.ReducedOrderSpline(fs, amp(fs, Mc), verbose=True)

print "\nPhase compression =", len(fs)/float(rs_phi.size)
print "Amplitude compression =", len(fs)/float(rs_amp.size)

def h(f):
  return rs_amp(f)*np.exp(1j*rs_phi(f))
